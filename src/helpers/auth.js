import axios from 'axios';

export function login (credentials) {
  return new Promise((res, rej) => {
    axios.post('/auth/login', credentials)
      .then((response) => {
        console.log(response);

        res(response.data);
      })
      .catch((error) => {
        rej(error);
      });
  });
}

export default function getLocalUser () {
  const userStr = localStorage.getItem('user');

  if (!userStr) {
    return null;
  }

  return JSON.parse(userStr);
}